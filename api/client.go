package api

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"


	logger "gitlab.com/golocazon/gologger"
	"io"
	"net/http"
	"net/url"
)

type Client struct {
	BaseURL     *url.URL
	UserAgent   string
	tokenString string

	httpClient *http.Client
}

func NewClient(httpClient *http.Client, baseURL *url.URL, userAgent string, secret string) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	tokenAuth := jwtauth.New("HS256", []byte(secret), nil)
	_, tokenString, _ := tokenAuth.Encode(jwt.MapClaims{"user_id": 1})

	logger.Infof("JWT Token %v", tokenString)
	c := &Client{httpClient: httpClient,
		BaseURL: baseURL, UserAgent: userAgent, tokenString: tokenString}
	return c
}

func (c *Client) NewRequest(method, path string, body interface{}) (*http.Request, error) {
	rel := &url.URL{Path: path}
	u := c.BaseURL.ResolveReference(rel)
	logger.Infof("Full URL %v", u)
	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			return nil, err
		}
	}
	req, err := http.NewRequest(method, u.String(), buf)
	if err != nil {
		return nil, err
	}
	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", c.UserAgent)
	req.Header.Set("Authorization", "Bearer "+c.tokenString)

	return req, nil
}
func (c *Client) Do(req *http.Request, v interface{}) (*http.Response, error) {
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return resp, errors.New("Bad response " + resp.Status)
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(v)
	return resp, err
}

func NewResponseFromError(err error) Response{
	return Response{
		Content: nil,
		Error:   &ResponseError{Detail: err.Error()},
	}
}
type ResponseError struct {
	Detail string `json:"detail"`
}

type Response struct {
	Content interface{}    `json:"content"`
	Error   *ResponseError `json:"error"`
}
