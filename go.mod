module gitlab.com/golocazon/gowsfwk

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.0.2+incompatible // indirect
	github.com/go-chi/jwtauth v4.0.3+incompatible
	gitlab.com/golocazon/gologger v0.0.3
)
